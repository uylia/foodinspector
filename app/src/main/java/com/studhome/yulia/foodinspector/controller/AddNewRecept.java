package com.studhome.yulia.foodinspector.controller;

import android.graphics.Bitmap;

import com.studhome.yulia.foodinspector.model.RecipeModel;

/**
 * Created by yuliasokolova on 03.11.16.
 */
public interface AddNewRecept {
    void onAddNewRecept(RecipeModel recipeModel);
}
