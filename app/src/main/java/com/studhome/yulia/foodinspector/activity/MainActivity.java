package com.studhome.yulia.foodinspector.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mingle.widget.LoadingView;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.NavigateMenuChangeInterface;
import com.studhome.yulia.foodinspector.fragment.ProductListFragment;
import com.studhome.yulia.foodinspector.fragment.ProfileFragment;
import com.studhome.yulia.foodinspector.fragment.RecipeFragment;
import com.studhome.yulia.foodinspector.manager.FireBaseConstants;
import com.studhome.yulia.foodinspector.manager.MeManager;
import com.studhome.yulia.foodinspector.model.Product;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NavigateMenuChangeInterface.OnChangeMenuItem {

    private Fragment fragment;
    private String tag = "";
    private MeManager meManager;
    private ShareLinkContent content;
    private CallbackManager callbackManager;
    private ArrayList<Product> mProductFBList;
    public static final String PRODUCT_LIST_TAG = "listFragment";
    public static final String COOK_FRAGMENT_TAG = "cookFragment";
    public static final String PROFILE_FRAGMENT_TAG = "profileFragment";
    public static final String RECEPT_FRAGMENT_TAG = "recipeFragment";

    @Bind(R.id.fb_share_button)
    ShareButton shareButton;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tv_title_toolbar)
    TextView title;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navigationView;
    @Bind(R.id.iv_toolbar_save)
    ImageView ivToolbarSave;
    @Bind(R.id.load_view)
    LinearLayout loadingView;


    // pass context to Calligraphy
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        meManager = new MeManager(this);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initNavigationLayout(toolbar);
        initShareFB();
        initLoadingViewLayout();

        saveIntent();
        fragment = ProductListFragment.getInstance(this, mProductFBList);
        title.setText("Список продуктов");
        openFragment(fragment, "listFragment");
    }


    private void initLoadingViewLayout() {
        loadingView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }


    private void saveIntent() {
        mProductFBList = new ArrayList<>();
        String jsonList = getIntent().getStringExtra(FireBaseConstants.PRODUCT_LIST);
        Type listType = new TypeToken<ArrayList<Product>>() {
        }.getType();
        mProductFBList = new Gson().fromJson(jsonList, (Type) listType);
    }


    public void showLoadingView() {
        loadingView.setVisibility(View.VISIBLE);
    }


    public void hideLoadingView() {
        loadingView.setVisibility(View.GONE);
    }


    public ArrayList<Product> getProductFBList() {
        return mProductFBList;
    }


    public ImageView getBtnSaveRecept() {
        return ivToolbarSave;
    }


    public void showSaveBtn() {
        ivToolbarSave.setVisibility(View.VISIBLE);
    }


    public void hideSaveBtn() {
        ivToolbarSave.setVisibility(View.GONE);
    }


    private void initShareFB() {
        content = new ShareLinkContent.Builder()
                .setContentTitle("I found cool app!")
                .setContentUrl(Uri.parse("http://google.com"))
                .setImageUrl(Uri.parse("http://www.clker.com/cliparts/a/c/4/9/1325314784688186076coock-hi.png"))
                .setContentDescription("Try cook something yourself!")
                .build();
        shareButton.setShareContent(content);

        shareButton.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(MainActivity.this, "Successfully posted", Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onCancel() {

            }


            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);

        VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {

            @Override
            public void onResult(VKAccessToken res) {
                res.saveTokenToSharedPreferences(MainActivity.this, "vk_token");

                new MeManager(MainActivity.this).setVkId(res.userId);
            }


            @Override
            public void onError(VKError error) {

            }
        });
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void initNavigationLayout(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.menu_icon);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_enabled},
                new int[]{android.R.attr.state_checked},
                new int[]{android.R.attr.state_pressed}
        };
        int[] colors = new int[]{
                Color.BLACK,
                Color.GREEN,
                Color.GREEN
        };
        ColorStateList csl = new ColorStateList(states, colors);
        navigationView.setItemTextColor(csl);
        navigationView.setItemIconTintList(csl);

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }


            @Override
            public void onDrawerOpened(View drawerView) {
                if (tag == RECEPT_FRAGMENT_TAG) {
                    if (fragment instanceof RecipeFragment) {
                        ((RecipeFragment) fragment).makeSideMenuEnable(false);
                    }
                }
            }


            @Override
            public void onDrawerClosed(View drawerView) {
                if (tag == RECEPT_FRAGMENT_TAG) {
                    if (fragment instanceof RecipeFragment) {
                        ((RecipeFragment) fragment).makeSideMenuEnable(true);
                    }
                }
            }


            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        drawerLayout.openDrawer(GravityCompat.START);
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        navigationView.getMenu().findItem(id).setChecked(true);

        switch (id) {
            case R.id.menu_food_list:
                if (tag.equals(PRODUCT_LIST_TAG)) break;
                fragment = ProductListFragment.getInstance(this, mProductFBList);
                setToolbarTitle(getResources().getString(R.string.product_list_title));
                hideSaveBtn();
                tag = PRODUCT_LIST_TAG;
                break;

            case R.id.menu_profile:
                if (tag.equals(PROFILE_FRAGMENT_TAG)) break;
                fragment = ProfileFragment.getInstance();
                setToolbarTitle(getResources().getString(R.string.profile_title));
                tag = PROFILE_FRAGMENT_TAG;
                hideSaveBtn();
                break;

            case R.id.menu_recipe:
                if (tag.equals(RECEPT_FRAGMENT_TAG)) break;
                fragment = RecipeFragment.getInstance(this);
                setToolbarTitle(getResources().getString(R.string.recept_title));
                showSaveBtn();
                tag = RECEPT_FRAGMENT_TAG;
                break;

            case R.id.menu_share:
                fragment = null;
                shareButton.performClick();
                hideSaveBtn();
                break;

            case R.id.menu_exit:
                meManager.setFbToken(null);
                meManager.setFbId(null);
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;

            default:
                fragment = ProductListFragment.getInstance(this, mProductFBList);
                setToolbarTitle(getResources().getString(R.string.product_list_title));
                hideSaveBtn();
        }
        if (fragment != null) {
            openFragment(fragment, tag);
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }


    public void openFragment(Fragment fragment, String tag) {
        //   getSupportFragmentManager().beginTransaction().replace(R.id.contentLayout, fragment, tag).addToBackStack(tag).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.contentLayout, fragment, tag).commit();
    }


    @Override
    public void ChangeMenuItem(int fragment) {
        navigationView.getMenu().findItem(fragment).setChecked(true);
        changeToolbar(fragment);
    }


    private void changeToolbar(int fragment) {
        switch (fragment) {
            case R.id.menu_food_list:
                setToolbarTitle(getResources().getString(R.string.product_list_title));
                hideSaveBtn();
                break;
//            case R.id.menu_cook:
//                setToolbarTitle(getResources().getString(R.string.cook_title));
//                showSaveBtn();
//                break;
            case R.id.menu_recipe:
                setToolbarTitle(getResources().getString(R.string.recept_title));
                hideSaveBtn();
                break;
        }
    }


    public void setToolbarTitle(String titleText) {
        title.setText(titleText);
    }

}
