package com.studhome.yulia.foodinspector.controller;

/**
 * Created by yuliasokolova on 30.01.17.
 */

public interface GetPhotoFirebaseUri {
    void onGetImageUri (String uri);
}
