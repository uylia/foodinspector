package com.studhome.yulia.foodinspector.controller;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.studhome.yulia.foodinspector.adapter.RecipeListAdapter;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.manager.FbManager;
import com.studhome.yulia.foodinspector.manager.ProductManager;
import com.studhome.yulia.foodinspector.model.Product;
import com.studhome.yulia.foodinspector.model.RecipeModel;
import com.studhome.yulia.foodinspector.util.GUI;
import com.studhome.yulia.foodinspector.util.GetPhotoUtil;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Julia on 01.02.2016.
 */
public class Dialog {
    private Context context;
    private EditText et_name, et_calories;
    private Spinner et_spinner;
    private String name, calories;
    private int spinnerChooseItem;
    private ProductManager productManager;

    private RecipeModel recipeModel;
    private ListView recipeList;
    private TextView tvWeight, tvCalorii;
    private RecipeListAdapter adapter;
    private EditText etRecipeName;
    private AddNewRecept saveListener;
    private View.OnClickListener callImgListener;
    private Button saveBtn;
    private CircleImageView cvReceptImage, cvCallImage;
    private GetPhotoUtil.LoadedPhoto imageRecept;

    // 1-fruits, 2-vegetables, 3-milk, 4-bakaleya, 5 - meat


    public Dialog(Context context) {
        this.context = context;
    }


    public void getAddProductDialog(final AddNewProduct listener) {
        final AlertDialog dialog = new AlertDialog.Builder(context).create();
        productManager = new ProductManager(context);

        View view = LayoutInflater.from(context).inflate(R.layout.layout_dialog_add_product, null);
        et_name = (EditText) view.findViewById(R.id.et_product_name);
        GUI.initFocusAndShowKeyboard(et_name, context);
        et_calories = (EditText) view.findViewById(R.id.et_product_calories);
        et_spinner = (Spinner) view.findViewById(R.id.spinner);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.categories_ru, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_layout_dropdown);
        et_spinner.setAdapter(adapter);
        et_spinner.setSelection(0);

        dialog.setView(view);
        dialog.show();
        dialog.setCancelable(true);
        view.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = et_name.getText().toString();
                if (name.length() == 0) {
                    Toast.makeText(context, "input name please", Toast.LENGTH_SHORT).show();
                    return;
                }

                calories = et_calories.getText().toString();
                try {
                    Integer.parseInt(calories);
                } catch (NumberFormatException e) {
                    Toast.makeText(context, "input number value please", Toast.LENGTH_SHORT).show();
                    return;
                }

                spinnerChooseItem = et_spinner.getSelectedItemPosition() + 1;

                Product p = new Product(name, calories, String.valueOf(R.drawable.tab_fruits), String.valueOf(spinnerChooseItem));
                productManager.addProduct(p);
                listener.onAddNewProduct(p);
                dialog.dismiss();
                Log.d("Log", "Item was added: name - " + name);
            }
        });
    }


    public AlertDialog getPickPhotoDialog(View.OnClickListener cameraListener, View.OnClickListener galleryListener) {
        final AlertDialog dialog = new AlertDialog.Builder(context).create();
        View view = LayoutInflater.from(context).inflate(R.layout.layout_dialog_pick_photo, null);

        dialog.setView(view);
        dialog.setCancelable(true);
        view.findViewById(R.id.btn_camera).setOnClickListener(cameraListener);
        view.findViewById(R.id.btn_gallery).setOnClickListener(galleryListener);

        dialog.show();
        return dialog;
    }


    public AlertDialog getAddReceptDialog(RecipeModel recipeData, AddNewRecept saveBtnListener, final View.OnClickListener callImageListener) {
        final AlertDialog dialog = new AlertDialog.Builder(context).create();
        View view = LayoutInflater.from(context).inflate(R.layout.layout_dialog_add_recipe, null);
        dialog.setView(view);
        dialog.setCancelable(true);

        this.saveListener = saveBtnListener;
        this.callImgListener = callImageListener;

        saveBtn = (Button) view.findViewById(R.id.btn_save_recept);
        recipeList = (ListView) view.findViewById(R.id.recipe_content);
        tvCalorii = (TextView) view.findViewById(R.id.tv_calorii);
        tvWeight = (TextView) view.findViewById(R.id.tv_weight);
        etRecipeName = (EditText) view.findViewById(R.id.et_recipe_name);
        cvReceptImage = (CircleImageView) view.findViewById(R.id.civ_image_recept);
        cvCallImage = (CircleImageView) view.findViewById(R.id.btn_add_photo);
        cvCallImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callImageListener.onClick(v);
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveRecept(dialog);
            }
        });

        fillLayout(recipeData);
        dialog.show();

        return dialog;
    }


    private void fillLayout(RecipeModel recipeData) {
        recipeModel = recipeData;
        adapter = new RecipeListAdapter(context);
        recipeList.setAdapter(adapter);
        adapter.updateRecipeAdapter(recipeModel.getProductList());

        tvWeight.setText(recipeModel.getWeightDish());
        tvCalorii.setText(recipeModel.getCaloriesDish());

        etRecipeName.setText(recipeModel.getNameRecept());
        if (recipeModel.getImageName() != null) {
            new FbManager(context).downloadPhotoByPhotoName(recipeModel.getImageName(), cvReceptImage);
        }
    }


    void onSaveRecept(AlertDialog dialog) {
        if (etRecipeName.getText().toString().trim().equals("")) {
            Toast.makeText(context, "Please, enter recipe name", Toast.LENGTH_SHORT).show();
        } else {
            recipeModel.setNameRecept(etRecipeName.getText().toString());
            saveListener.onAddNewRecept(recipeModel);
            dialog.dismiss();
        }
    }


    public void setImage(GetPhotoUtil.LoadedPhoto image) {
        this.imageRecept = image;
        cvReceptImage.setImageBitmap(imageRecept.bitmap);
    }
}