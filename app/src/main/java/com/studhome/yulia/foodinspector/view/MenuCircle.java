package com.studhome.yulia.foodinspector.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.OnContextItemClick;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yuliasokolova on 24.02.17.
 */

public class MenuCircle extends FrameLayout {

    @Bind(R.id.menu_icon)
    LinearLayout menuIcon;

    @Bind(R.id.recipe_context_menu)
    LinearLayout menuLayout;

    @Bind(R.id.tv_menu_edit)
    TextView tvMenuEdit;

    @Bind(R.id.tv_menu_delete)
    TextView getTvMenuDelete;


    private Animation scaleOutAnim, scaleInAnim;
    private OnContextItemClick listener;
    private boolean isMenuShowed = false;

    public MenuCircle(Context context) {
        super(context);
        initView();
    }


    public MenuCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    public MenuCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    private void initView() {
        inflate(getContext(), R.layout.menu_layout, this);
        ButterKnife.bind(this);
        scaleOutAnim = AnimationUtils.loadAnimation(super.getContext(), R.anim.scale_out);
        scaleInAnim = AnimationUtils.loadAnimation(super.getContext(), R.anim.scale_in);
        scaleInAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }


            @Override
            public void onAnimationEnd(Animation animation) {
                menuLayout.setVisibility(GONE);
            }


            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }


    @OnClick(R.id.menu_icon)
    void onContextIconClick() {
        if (isMenuShowed){
            hideCustomContextMenu();
        } else {
            showCustomContextMenu();
        }
    }


    @OnClick(R.id.tv_menu_edit)
    void onEditClick(){
        if(listener != null){
            listener.onEditClick();
        }
    }


    @OnClick(R.id.tv_menu_delete)
    void onDeleteClick(){
        if(listener != null){
            listener.onDelClick();
        }
    }

    public void showCustomContextMenu() {
        menuLayout.setVisibility(VISIBLE);
        menuLayout.startAnimation(scaleOutAnim);
        isMenuShowed = true;
    }


    public void hideCustomContextMenu() {
        isMenuShowed = false;
        menuLayout.startAnimation(scaleInAnim);
    }


    public void setListener(OnContextItemClick listener){
        this.listener = listener;
    }
}
