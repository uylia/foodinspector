package com.studhome.yulia.foodinspector.util;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by yuliasokolova on 09.03.17.
 */

public class BottomSpacingRecView extends RecyclerView.ItemDecoration {

    private int space;

    public BottomSpacingRecView(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(parent.getAdapter().getItemCount() > 0 && parent.getChildAdapterPosition(view) == state.getItemCount()-1){
            outRect.bottom = space;
            outRect.top = 0;
        }
    }
}