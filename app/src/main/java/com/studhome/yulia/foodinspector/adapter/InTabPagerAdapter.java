package com.studhome.yulia.foodinspector.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.studhome.yulia.foodinspector.controller.GridProductsClickInterface;
import com.studhome.yulia.foodinspector.fragment.ContentGridFragment;
import com.studhome.yulia.foodinspector.model.Product;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 21.10.16.
 */
public class InTabPagerAdapter extends FragmentStatePagerAdapter {
    private static final int GRID_SIZE = 6;
    ArrayList<Product> productsList = new ArrayList<>();
    GridProductsClickInterface.OnClickGridItem onClicklistener;
    public InTabPagerAdapter(FragmentManager fm, ArrayList<Product> productsList, GridProductsClickInterface.OnClickGridItem onClicklistener) {
        super(fm);
        this.productsList = productsList;
        this.onClicklistener = onClicklistener;
    }

    @Override
    public Fragment getItem(int position) {
        //make list for current pager view
        ArrayList<Product> currentList = new ArrayList<>();
        for (int i = position * GRID_SIZE; i < (position + 1) * GRID_SIZE; i++) {
            if (i < productsList.size()) currentList.add(productsList.get(i));
            else break;
        }
        return ContentGridFragment.getInstance(currentList, onClicklistener);
    }


    @Override
    public int getCount() {
        return productsList.size()/GRID_SIZE + 1;
    }
}
