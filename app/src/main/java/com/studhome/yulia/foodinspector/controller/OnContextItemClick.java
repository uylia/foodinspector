package com.studhome.yulia.foodinspector.controller;

/**
 * Created by yuliasokolova on 07.03.17.
 */

public interface OnContextItemClick {
    void onEditClick();
    void onDelClick();
}
