package com.studhome.yulia.foodinspector.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.AddNewProduct;
import com.studhome.yulia.foodinspector.controller.Dialog;
import com.studhome.yulia.foodinspector.controller.NavigateMenuChangeInterface;
import com.studhome.yulia.foodinspector.adapter.ProductListAdapter;
import com.studhome.yulia.foodinspector.manager.ProductManager;
import com.studhome.yulia.foodinspector.model.Product;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Julia on 29.01.2016.
 */
public class ProductListFragment extends Fragment implements AddNewProduct{

    private ProductManager productDB;
    private NavigateMenuChangeInterface.OnChangeMenuItem onChangeMenuItemListener;


    ProductListAdapter adapter;
    List<Product>  products;

    @Bind(R.id.lv_product_list)
    ListView listView;

    @Bind(R.id.floating_button_add_new_product)
    FloatingActionButton fab;

    public static ProductListFragment getInstance(NavigateMenuChangeInterface.OnChangeMenuItem listener, ArrayList<Product> productFB_list) {
        ProductListFragment  fragment = new ProductListFragment ();
        fragment.onChangeMenuItemListener = listener;
        fragment.products = productFB_list;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productDB = new ProductManager(getActivity());//read my own added products
        products.addAll(productDB.getProductList());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_product_list, container, false);
        ButterKnife.bind(this,v);

        if (onChangeMenuItemListener != null) onChangeMenuItemListener.ChangeMenuItem(R.id.menu_food_list);

        adapter = new ProductListAdapter(getActivity());
        listView.setAdapter(adapter);
        adapter.updateList(products);

        return v;
    }


    @OnClick (R.id.floating_button_add_new_product)
    void onFABClick (){
        new Dialog(getActivity()).getAddProductDialog(this);
    }


    @Override
    public void onAddNewProduct(Product product) {
        if (adapter != null)
            adapter.addNewItem(product);
    }
}
