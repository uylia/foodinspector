package com.studhome.yulia.foodinspector.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.model.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Julia on 29.01.2016.
 */
public class ProductListAdapter extends BaseAdapter {
    Context context;
    List<Product> products;
    LayoutInflater inflater;

    public ProductListAdapter(Context context) {
        this.context = context;
        products = new ArrayList<Product>();
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    private List<Product> sortProductsList (List<Product> prodList){
        List<Product> prodRemoveList = new ArrayList<>();

        Collections.sort(prodList, new Comparator<Product>() {
            @Override
            public int compare(Product lhs, Product rhs) {
                return (lhs.getName().toUpperCase()).compareTo(rhs.getName().toUpperCase());
            }
        });

        for (int i = 0; i< prodList.size(); i++) {
            int y = i + 1;
            if (y < prodList.size() && prodList.get(i).getName().equals(prodList.get(y).getName())){
                prodRemoveList.add(prodList.get(i));
            }
        }

        for (int i = 0; i< prodRemoveList.size(); i++){
            prodList.remove(prodRemoveList.get(i));
        }

        return prodList;
    }

    @Override
    public Product getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateList (List<Product> prodList){

        products = sortProductsList(prodList);
        notifyDataSetChanged();
    }

    public void addNewItem (Product newProduct){
        products.add (newProduct);
        products = sortProductsList(products);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_lv_product_list_fragment, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

            Product product = getItem(position);
            holder.name.setText(product.getName());
            holder.calories.setText(product.getCalorii());
            return convertView;
    }

    private class ViewHolder {
        TextView name, calories;

        public ViewHolder(View v) {
            name = (TextView) v.findViewById(R.id.tv_product_name);
            calories = (TextView) v.findViewById(R.id.tv_product_callories);

        }

    }
}
