package com.studhome.yulia.foodinspector.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.mingle.widget.LoadingView;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.OnReadProductList;
import com.studhome.yulia.foodinspector.manager.FbManager;
import com.studhome.yulia.foodinspector.manager.FireBaseConstants;
import com.studhome.yulia.foodinspector.manager.MeManager;

import butterknife.Bind;
import butterknife.ButterKnife;

import com.studhome.yulia.foodinspector.model.Product;
import com.studhome.yulia.foodinspector.model.UserModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by yuliasokolova on 24.10.16.
 */
public class SplashActivity extends AppCompatActivity {

    @Bind(R.id.login_button)
    LoginButton loginButton;

    @Bind(R.id.my_load_view)
    LoadingView loadingView;

    private CallbackManager callbackManager;
    private MeManager meManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String mFacebookToken, mFacebookId;
    private AccessToken accessToken;
    private FbManager mFbManager;
    private  UserModel userModel;
    private ArrayList<Product> mProductFBList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.splash_activity);
        ButterKnife.bind(this);

      //  Fabric.with(this, new Crashlytics());

        mFbManager = new FbManager(SplashActivity.this);
        mAuth = FirebaseAuth.getInstance();

        meManager = new MeManager(this);
        mFacebookToken = meManager.getFbToken();
        if (mFacebookToken == null)
            initFB();
        else
            readFireBaseDB();
// called after signin in firebase
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (accessToken != null && mFacebookId != null){
                        userModel = new UserModel();
                        userModel.setUserEmail(user.getEmail());
                        addUserToDbIfNotExist(accessToken, mFacebookId);
                    }
                }
            }
        };

    }

//    public void printHashKey() {
//        try {
//            PackageInfo info;
//            String packageName = getApplicationContext().getPackageName();
//
//            //Retriving package info
//            info = getPackageManager().getPackageInfo(packageName,
//                    PackageManager.GET_SIGNATURES);
//
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String hashKey = new String(Base64.encode(md.digest(), 0));
//                Log.i("TAG", "printHashKey() Hash Key: " + hashKey);
//            }
//        } catch (NoSuchAlgorithmException e) {
//
//        } catch (Exception e) {
//        }
//    }

    private void enterApp(ArrayList<Product> list) {
        loadingView.setVisibility(View.GONE);
        startActivity(new Intent(SplashActivity.this, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .putExtra(FireBaseConstants.PRODUCT_LIST, new Gson().toJson(list)));
    }


    private void readFireBaseDB() {
        loginButton.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);
        mFbManager.readAllUsers();
        mFbManager.readProductList(new OnReadProductList() {
            @Override
            public void OnSuccessRead(ArrayList<Product> list) {
                enterApp(list);
            }
        });

    }


    private void initFB() {
        loginButton.setReadPermissions(Arrays.asList("email", "user_photos", "user_birthday", "public_profile"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                mFacebookToken = accessToken.getToken();
                mFacebookId = loginResult.getAccessToken().getUserId();
                handleFacebookAccessToken();

            }


            @Override
            public void onCancel() {
                Toast.makeText(SplashActivity.this, "auth was canceled", Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onError(FacebookException error) {
                Toast.makeText(SplashActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void handleFacebookAccessToken() {
        AuthCredential credential = FacebookAuthProvider.getCredential(mFacebookToken);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(SplashActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        meManager.setFbToken(mFacebookToken);
                        meManager.setFbId(mFacebookId);
                        readFireBaseDB();
                        LoginManager.getInstance().logOut();
                    }
                });

    }


    private void addUserToDbIfNotExist(final AccessToken token, final String mFacebookId) {
        GraphRequest.newMeRequest(token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject user,
                            GraphResponse response) {
                        JSONObject jobject = response.getJSONObject();
                        String id  = jobject.optString("id");
                        String firstName = jobject.optString("name");
                        String imageURL = "https://graph.facebook.com/" + id + "/picture?type=large";

                        userModel.setUserName(firstName);
                        userModel.setUserImageUrl(imageURL);
                        userModel.setUserFacebookId(mFacebookId);
                  //      userModel.setMyFriend(false); // false - is not my friend
                        mFbManager.addUserToDb(userModel);
                        meManager.setUserImage(imageURL);
                    }
                }).executeAsync();


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}