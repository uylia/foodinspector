package com.studhome.yulia.foodinspector.controller;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by yuliasokolova on 22.02.17.
 */
public interface OnGetBitmapListener {
    void onGetBitmap(Bitmap bitmap);
}
