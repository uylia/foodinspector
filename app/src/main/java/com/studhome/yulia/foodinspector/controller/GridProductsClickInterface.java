package com.studhome.yulia.foodinspector.controller;

import android.view.View;

import com.studhome.yulia.foodinspector.model.Product;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Julia on 30.01.2016.
 */
public class GridProductsClickInterface {
    public interface  OnClickGridItem{
        void onClick(Product product, CircleImageView bg, View view);
    }
}
