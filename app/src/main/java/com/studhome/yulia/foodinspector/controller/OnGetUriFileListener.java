package com.studhome.yulia.foodinspector.controller;

import android.net.Uri;

/**
 * Created by yuliasokolova on 22.02.17.
 */
public interface OnGetUriFileListener {
    void onGetUri(Uri uri);
}
