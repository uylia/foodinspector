package com.studhome.yulia.foodinspector.manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by yuliasokolova on 31.10.16.
 */
public class UserSharePrefs {
    private static volatile UserSharePrefs instance;
    private static SharedPreferences sp;
    private String FB_TOKEN = "facebook_token";
    private String FB_USER_ID = "facebook_id";
    private String VK_TOKEN = "vk_token";
    private String VK_USER_ID = "vk_id";
    private String FB_USER_IMAGE = "fb_user_image";
    private static String USER_SP = "user_sp";

    private static UserSharePrefs createConstructor(Context context){
        instance = new UserSharePrefs();
        initSharePrefs(context);
        return instance;
    }

    private static void initSharePrefs(Context context) {
        sp = context.getSharedPreferences(USER_SP, Context.MODE_PRIVATE);
    }

    public static UserSharePrefs getInstance(Context context) {
        UserSharePrefs localInstance = instance;
        if (localInstance == null) {
            synchronized (UserSharePrefs.class) {
                    instance = localInstance = createConstructor(context);
            }
        }
        return localInstance;
    }

    public void setFBToken(String token) {
        sp.edit().putString(FB_TOKEN, token).commit();
    }

    public String getFBToken(){
        return sp.getString(FB_TOKEN, null);
    }


    public void setFBId(String id) {
        sp.edit().putString(FB_USER_ID, id).commit();
    }

    public String getFBId(){
        return sp.getString(FB_USER_ID, null);
    }


    public void setVkId(String id) {
        sp.edit().putString(VK_USER_ID, id).commit();
    }

    public String getVkId (){
        return sp.getString(VK_USER_ID, null);
    }

    public void setFbImageUrl(String imageUrl) {
        sp.edit().putString(FB_USER_IMAGE, imageUrl).commit();
    }

    public String getFbImageUrl (){
        return sp.getString(FB_USER_IMAGE, null);
    }
}
