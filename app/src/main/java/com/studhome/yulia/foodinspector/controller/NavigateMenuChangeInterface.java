package com.studhome.yulia.foodinspector.controller;

/**
 * Created by Julia on 30.01.2016.
 */
public class NavigateMenuChangeInterface {
    public interface  OnChangeMenuItem{
        void ChangeMenuItem(int fragmenr);
    }
}
