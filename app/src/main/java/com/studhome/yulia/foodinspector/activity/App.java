package com.studhome.yulia.foodinspector.activity;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.studhome.yulia.foodinspector.R;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by yuliasokolova on 27.01.17.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
         //init vk.com
        VKSdk.initialize(getApplicationContext());
         //initalize Calligraphy
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("Opificio_regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        //init tw
         initTwitter();
    }



    private void initTwitter(){
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(getApplicationContext().getResources().getString(R.string.twitter_sdk_app_key),
                getApplicationContext().getResources().getString(R.string.twitter_sdk_app_secret));

        Fabric.with(getApplicationContext(), new TwitterCore (authConfig), new Crashlytics(), new TweetComposer());
    }

}
