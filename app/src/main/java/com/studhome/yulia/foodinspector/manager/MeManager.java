package com.studhome.yulia.foodinspector.manager;

import android.content.Context;

/**
 * Created by yuliasokolova on 31.10.16.
 */
public class MeManager {
    private Context context;
    private UserSharePrefs sp;

    public MeManager(Context context) {
        this.context = context;

        sp = UserSharePrefs.getInstance(context);
    }

    public void setFbToken(String token) {
        sp.setFBToken(token);
    }

    public String getFbToken() {
        return sp.getFBToken();
    }


    public void setFbId(String id) {
        sp.setFBId(id);
    }

    public String getFbId() {
        return sp.getFBId();
    }

    public void setVkId(String id) {
        sp.setVkId(id);
    }

    public String getVkId() {
        return sp.getVkId();
    }

    public void setUserImage(String image) {
        sp.setFbImageUrl(image);
    }

    public String getUserImage() {
        return sp.getFbImageUrl();
    }

}
