package com.studhome.yulia.foodinspector.manager;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.DBHelper;
import com.studhome.yulia.foodinspector.model.Product;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuliasokolova on 03.11.16.
 */
public class ProductManager {
    Context context;
    DBHelper helper;
    RuntimeExceptionDao simpleDao;

    public ProductManager(Context context){
        this.context = context;
        helper = new DBHelper(context);
        try {
            simpleDao = helper.getSimpleDataDao();
        } catch (SQLException e) {
            Log.d("LogError", "error simple dao: " + e.getMessage());
        }
    }


    //method for list of products
    public ArrayList<Product> getProductList() {
        List<Product> list = simpleDao.queryForAll();
        return (ArrayList)list;
    }

    //list for each tab
    public ArrayList<Product> getTypeProductList (ArrayList<Product> products, int category){
        ArrayList <Product> allProducts = products;
        ArrayList <Product> tabProducts = new ArrayList<>();
        for(Product p: allProducts){
            if (p.getCategor().equals(String.valueOf(category + 1)))
                tabProducts.add(p);
        }
        return tabProducts;
    }

    //method for insert data
    public void addProduct(Product product) {
        simpleDao.create(product);
    }

    private void addListToDB (List<Product> list){
        for (Product p: list){
            simpleDao.create(p);
        }
    }

}
