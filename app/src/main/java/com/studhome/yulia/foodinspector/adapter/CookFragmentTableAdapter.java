package com.studhome.yulia.foodinspector.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.RecyclerViewDragSwipeHelper;
import com.studhome.yulia.foodinspector.controller.onUpdateReceptData;
import com.studhome.yulia.foodinspector.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by sergey on 25.03.2016.
 */
public class CookFragmentTableAdapter extends RecyclerView.Adapter<CookFragmentTableAdapter.MyViewHolder>
        implements RecyclerViewDragSwipeHelper {
    private ArrayList<Product> myList = new ArrayList();
    private LayoutInflater inflater;
    private Context context;

    private HashMap<Integer, Integer> weightMap = new HashMap();
    private HashMap<Integer, Float> caloriesMap = new HashMap();
    private HashMap<Integer, Float> belkiMap = new HashMap();
    private HashMap<Integer, Float> zuriMap = new HashMap();
    private HashMap<Integer, Float> uglevodiMap = new HashMap();

    private onUpdateReceptData onUpdateUiListener;


    public CookFragmentTableAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }


    public void addNewItem(Product product) {
        myList.add(product);
        notifyDataSetChanged();
    }


    public void addListItem(ArrayList<Product> productList) {
        myList.clear();
        myList.addAll(productList);
        notifyDataSetChanged();
    }


    private Product getItem(int position) {
        return myList.get(position);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.layout_table, parent, false);
        return new MyViewHolder(convertView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Product productTableItem = getItem(position);
        holder.pltName.setText(productTableItem.getName());
        holder.pltWeight.setTag(position);
        int weight = productTableItem.getWeight();
        if (weight > 0) {
            holder.pltWeight.setText(String.valueOf(weight));
        } else {
            holder.pltWeight.setText("");
        }
        addToMaps(position, productTableItem.getWeight());
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public int getItemCount() {
        return myList.size();
    }


    public ArrayList<Product> getMyList() {
        return myList;
    }


    public String getDishWeight() {
        return getCalculatedValueWeight();
    }


    public String getDishCalories() {
        return getCalculatedValueFloat(caloriesMap);
    }


    public String getDishBelki() {
        return getCalculatedValueFloat(belkiMap);
    }


    public String getDishZuri() {
        return getCalculatedValueFloat(zuriMap);
    }


    public String getDishUglevodi() {
        return getCalculatedValueFloat(uglevodiMap);
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(myList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(myList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }


    @Override
    public void onItemDismiss(int position) {
        myList.remove(position);
        removeFromMaps(position);
        notifyItemRemoved(position);
        updateUi();
    }


    public void setUiListener(onUpdateReceptData onUpdateReceptData) {
        onUpdateUiListener = onUpdateReceptData;
    }


    private void updateUi() {
        if (onUpdateUiListener != null) {
            onUpdateUiListener.getWeight(getDishWeight());
            onUpdateUiListener.getCalorii(getDishCalories());
            onUpdateUiListener.getBelki(getDishBelki());
            onUpdateUiListener.getZuri(getDishZuri());
            onUpdateUiListener.getUglevodi(getDishUglevodi());
        }
    }


    private String getCalculatedValueFloat(HashMap<Integer, Float> dataMap) {
        float calculatedValue = 0.0f;
        for (Float value : dataMap.values()) {
            calculatedValue += value;
        }
        BigDecimal bd = new BigDecimal(Float.toString(calculatedValue));
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        return String.valueOf(bd.floatValue());
    }


    private String getCalculatedValueWeight() {
        int calculatedValueWeight = 0;
        for (Integer weight : weightMap.values()) {
            calculatedValueWeight += weight;
        }
        return String.valueOf(calculatedValueWeight);
    }


    private void addToMaps(int position, int weight) {
        weightMap.put(position, weight);
        caloriesMap.put(position, getValueForProduct(Float.parseFloat(getItem(position).getCalorii()), weight));
        belkiMap.put(position, getValueForProduct(Float.parseFloat(getItem(position).getBelki()), weight));
        zuriMap.put(position, getValueForProduct(Float.parseFloat(getItem(position).getZuri()), weight));
        uglevodiMap.put(position, getValueForProduct(Float.parseFloat(getItem(position).getUglevodi()), weight));
    }


    private float getValueForProduct(float valueFor100Gramm, int weight) {
        return valueFor100Gramm * weight / 100;
    }


    private void removeFromMaps(int position) {
        if (weightMap.containsKey(position) && caloriesMap.containsKey(position) &&
                belkiMap.containsKey(position) && zuriMap.containsKey(position) && uglevodiMap.containsKey(position)) {
            weightMap.remove(position);
            caloriesMap.remove(position);
            belkiMap.remove(position);
            zuriMap.remove(position);
            uglevodiMap.remove(position);
        }
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView pltName, pltWeight;
        int inputValueWeight;


        public MyViewHolder(View item) {
            super(item);
            pltName = (TextView) item.findViewById(R.id.tv_product);
            pltWeight = (TextView) item.findViewById(R.id.tv_wes);
            pltWeight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }


                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int currentPosition = (int) pltWeight.getTag();

                    if (s.toString().equals("") || s.toString().equals("0")) {
                        inputValueWeight = 0;
                        removeFromMaps(currentPosition);
                    } else {
                        inputValueWeight = Integer.parseInt(s.toString().trim());
                        addToMaps(currentPosition, inputValueWeight);
                    }

                    saveProductWeight(inputValueWeight, currentPosition);

                    updateUi();
                }


                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }


    }


    private void saveProductWeight(int inputValueWeight, int position) {
        Product product = getItem(position);
        product.setWeight(inputValueWeight);
        getMyList().set(position, product);
    }
}
