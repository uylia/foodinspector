package com.studhome.yulia.foodinspector.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 17.01.17.
 */

public class RecipeNameListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> list;
    private View prevView = null, lastView = null;

    public RecipeNameListAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }


    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public String getItem(int position) {
        return list.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NameHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, null);
            holder = new NameHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (NameHolder) convertView.getTag();
        }
        holder.tvName.setText(list.get(position));
        
        if (position == 0) {
            prevView = convertView;
       //     convertView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        }

        if (position == list.size() - 1){
            lastView = convertView;
        }

        return convertView;
    }


    public View getPrevView() {
        return prevView;
    }

    public View getLastView(){
        return lastView;
    }


    public void updateList(ArrayList<String> recipeNamesList) {
        this.list = recipeNamesList;
        notifyDataSetChanged();
    }


    private class NameHolder {
        TextView tvName;


        public NameHolder(View convertView) {
            tvName = (TextView) convertView.findViewById(android.R.id.text1);
        }
    }
}
