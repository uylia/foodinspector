package com.studhome.yulia.foodinspector.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.studhome.yulia.foodinspector.adapter.InTabPagerAdapter;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.GridProductsClickInterface;
import com.studhome.yulia.foodinspector.manager.ProductManager;
import com.studhome.yulia.foodinspector.model.Product;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 10.10.16.
 */
public class TabProductFragment extends Fragment{
    ViewPager pager;
    CirclePageIndicator indicator;
    ArrayList <Product> productsList;
    ArrayList <Product> allProducts;
    GridProductsClickInterface.OnClickGridItem onClicklistener;
    private static String TAB_POSITION ;
    private int position;
    private ProductManager productDB;

    public static Fragment getInstance(int position, GridProductsClickInterface.OnClickGridItem onClicklistener, ArrayList<Product> mProductFBList){
        TabProductFragment fragment = new TabProductFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TAB_POSITION, position);
        fragment.setArguments(bundle);
        fragment.onClicklistener = onClicklistener;
        fragment.allProducts = mProductFBList;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productDB = new ProductManager(getActivity());
        position = getArguments().getInt(TAB_POSITION, 0);
        productsList = productDB.getTypeProductList(allProducts,position);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_product_list, null);
        pager = (ViewPager) view.findViewById(R.id.inner_pager);
        pager.setAdapter(new InTabPagerAdapter(getChildFragmentManager(), productsList, onClicklistener));
        indicator = (CirclePageIndicator) view.findViewById(R.id.vp_indicator);
        indicator.setViewPager(pager, 0);
        return view;
    }

}
