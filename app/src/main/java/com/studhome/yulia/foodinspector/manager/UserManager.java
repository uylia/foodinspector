package com.studhome.yulia.foodinspector.manager;

import com.studhome.yulia.foodinspector.model.UserModel;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 09.02.17.
 */

public class UserManager {

    private static UserManager instance;

    private ArrayList<UserModel> allUsersList;


    private static UserManager createConstructor(){
        instance = new UserManager();
        return  instance;
    }

    public static UserManager getInstance(){
        if (instance == null){
            instance = createConstructor();
        }
        return  instance;
    }


    public ArrayList<UserModel> getAllUsersList() {

        return allUsersList;
    }


    public void setAllUsersList(ArrayList<UserModel> allUsersList) {
        this.allUsersList = allUsersList;
    }


}
