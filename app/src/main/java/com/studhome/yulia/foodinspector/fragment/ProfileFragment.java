package com.studhome.yulia.foodinspector.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.manager.MeManager;
import com.studhome.yulia.foodinspector.util.GUI;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yuliasokolova on 16.03.17.
 */

public class ProfileFragment extends Fragment {

    @Bind(R.id.iv_profile_avatar)
    ImageView ivProfileImage;
    @Bind(R.id.et_profile_name)
    EditText etName;
    @Bind(R.id.et_profile_age)
    EditText etAge;
    @Bind(R.id.et_profile_height)
    EditText etHeight;
    @Bind(R.id.et_profile_weight)
    EditText etWeight;
    @Bind(R.id.card_et)
    CardView cardEditText;
    @Bind(R.id.card_rb)
    CardView cardRadioBtn;
    @Bind(R.id.btn_calculate)
    Button btnCalculate;
    @Bind(R.id.rb_female)
    RadioButton rbFemale;
    @Bind(R.id.rb_male)
    RadioButton rbMale;
    @Bind(R.id.tv_daily_cal_norm)
    TextView tvDailyCaloriesNorm;
    @Bind(R.id.tv_daily_cal_lose)
    TextView tvDailyCaloriesLose;

    private Context context;
    private float userBmr, userType;
    private int dailyCalories, dailyForLoseCalories;

    private static final float TYPE_1 = 1.2f;
    private static final float TYPE_2 = 1.38f;
    private static final float TYPE_3 = 1.56f;
    private static final float TYPE_4 = 1.73f;
    private static final float TYPE_5 = 1.95f;

    private int userWeight, userAge, userHeight;
    private int userGender; // 0 - male, 1 - female


    public static ProfileFragment getInstance() {
        return new ProfileFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_profile_fragment, null);
        ButterKnife.bind(this, view);

        context = getActivity();

        initTouchListeners();
        setUserImage();
        return view;
    }

    private void setUserImage() {
        MeManager meManager = new MeManager(getActivity());
        Picasso.with(getActivity()).load(Uri.parse(meManager.getUserImage())).into(ivProfileImage);
    }


    private void initTouchListeners() {
        cardEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (GUI.isKeyboardVisible(v, context)) {
                    GUI.hideKeyboard((Activity) context);
                }
                return true;
            }
        });
        cardRadioBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (GUI.isKeyboardVisible(v, context)) {
                    GUI.hideKeyboard((Activity) context);
                }
                return true;
            }
        });
    }


    @OnClick(R.id.btn_calculate)
    void onCalculateClick() {
        userAge = Integer.parseInt(etAge.getText().toString());
        userWeight = Integer.parseInt(etWeight.getText().toString());
        userHeight = Integer.parseInt(etHeight.getText().toString());
        userGender = rbFemale.isChecked() ? 1 : 0;
        userBmr = getBasalMetabolicRate(userAge, userHeight, userWeight, userGender);
        userType = getPhysicalType();
        dailyCalories = Math.round(userBmr * userType);
        dailyForLoseCalories = getValueLoseCalories(dailyCalories);
        tvDailyCaloriesNorm.setText(String.valueOf(dailyCalories));
        tvDailyCaloriesLose.setText(String.valueOf(dailyForLoseCalories));
    }


    private float getPhysicalType() {
        int rbId = 0;
//// FIXME: 05.06.2017 
        switch (rbId) {
            case R.id.rb_type1: {
                return TYPE_1;
            }
            case R.id.rb_type2: {
                return TYPE_2;
            }
            case R.id.rb_type3: {
                return TYPE_3;
            }
            case R.id.rb_type4: {
                return TYPE_4;
            }
            case R.id.rb_type5: {
                return TYPE_5;
            }
            default:
                return TYPE_2;
        }
    }


    private float getBasalMetabolicRate(int age, int height, int weight, int gender) {
        int k = (gender == 1) ? 655 : 66;
        float nWeight = (gender == 1) ? 9.6f : 13.7f;
        float nHeight = (gender == 1) ? 1.8f : 5f;
        float nAge = (gender == 1) ? 4.7f : 6.8f;
        return k + (nWeight * weight) + (nHeight * height) - (nAge * age);
    }


    private int getValueLoseCalories(int dailyCalories) {
        float dietConstant = dailyCalories * 0.2f;
        return Math.round(dailyCalories - dietConstant);
    }

}
