package com.studhome.yulia.foodinspector.model;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yuliasokolova on 07.11.16.
 */
public class RecipeModel {
    private String nameRecept;
    private ArrayList<Product> productList = null;
    private String weightDish;
    private String caloriesDish;
    private String belkiDish;
    private String zuriDish;
    private String uglevodiDish;
    private String imageName = "";
    private String key;


    public RecipeModel() {
    }


    public void setNameRecept(String nameRecept) {
        this.nameRecept = nameRecept;
    }


    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }


    public void setWeightDish(String weightDish) {
        this.weightDish = weightDish;
    }


    public void setCaloriesDish(String caloriesDish) {
        this.caloriesDish = caloriesDish;
    }


    public String getCaloriesDish() {
        return caloriesDish;
    }


    public String getWeightDish() {
        return weightDish;
    }


    public ArrayList<Product> getProductList() {
        return productList;
    }


    public String getNameRecept() {
        return nameRecept;
    }


    public String getImageName() {
        return imageName;
    }


    public void setImageName(String imageName) {
        this.imageName = imageName;
    }


    public String getKey() {
        return key;
    }


    public void setKey(String key) {
        this.key = key;
    }


    public String getBelkiDish() {
        return belkiDish;
    }


    public void setBelkiDish(String belkiDish) {
        this.belkiDish = belkiDish;
    }


    public String getZuriDish() {
        return zuriDish;
    }


    public void setZuriDish(String zuriDish) {
        this.zuriDish = zuriDish;
    }


    public String getUglevodiDish() {
        return uglevodiDish;
    }


    public void setUglevodiDish(String uglevodiDish) {
        this.uglevodiDish = uglevodiDish;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("nameRecept", nameRecept);
        result.put("weightDish", weightDish);
        result.put("caloriesDish", caloriesDish);
        result.put("belkiDish", belkiDish);
        result.put("zuriDish", zuriDish);
        result.put("uglevodiDish", uglevodiDish);
        result.put("imageName", imageName);
        result.put("productList", productList);
        return result;
    }

}
