package com.studhome.yulia.foodinspector.controller;

import com.studhome.yulia.foodinspector.model.Product;
import com.studhome.yulia.foodinspector.model.RecipeModel;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 16.01.17.
 */

public interface OnReadRecipeList {
    void OnSuccessRead(ArrayList<RecipeModel> list);
}
