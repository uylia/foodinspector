package com.studhome.yulia.foodinspector.controller;

import android.graphics.Bitmap;

import com.studhome.yulia.foodinspector.model.Product;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 16.01.17.
 */

public interface OnReadReceptPhoto {
    void OnSuccessRead(Bitmap photo);
}
