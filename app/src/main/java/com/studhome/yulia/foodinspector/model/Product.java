package com.studhome.yulia.foodinspector.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Julia on 29.01.2016.
 */
@DatabaseTable(tableName = "products")
public class Product {
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String calorii;
    @DatabaseField
    private String belki;
    @DatabaseField
    private String zuri;
    @DatabaseField
    private String uglevodi;
    @DatabaseField
    private String image_url;
    @DatabaseField
    private String categor; // 1-fruits, 2-vegetables, 3-milk, 4-bakaleya, 5 - meat

    private int weight;


    public String getCategor() {
        return categor;
    }


    public String getImage_url() {
        return image_url;
    }


    public String getName() {
        return name;
    }


    public String getCalorii() {
        return calorii;
    }


    public int getWeight() {
        return weight;
    }


    public String getBelki() {
        return belki;
    }


    public String getZuri() {
        return zuri;
    }


    public String getUglevodi() {
        return uglevodi;
    }


    public void setWeight(int weight) {
        this.weight = weight;
    }


    public Product() {
    }


    public Product(String name, String calorii, String image_url, String categor) {
        this.name = name;
        this.calorii = calorii;
        this.image_url = image_url;
        this.categor = categor;
    }


    //constructor for recipe list
    public Product(String name, int weight_product) {
        this.name = name;
        this.weight = weight_product;
    }

}
