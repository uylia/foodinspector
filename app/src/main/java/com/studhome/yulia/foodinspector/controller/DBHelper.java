package com.studhome.yulia.foodinspector.controller;

        import android.util.Log;
        import java.sql.SQLException;

        import android.content.Context;
        import android.database.sqlite.SQLiteDatabase;
        import com.studhome.yulia.foodinspector.model.Product;
        import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
        import com.j256.ormlite.dao.RuntimeExceptionDao;
        import com.j256.ormlite.support.ConnectionSource;
        import com.j256.ormlite.table.TableUtils;


/**
 * Created by Julia on 30.01.2016.
 */
public class DBHelper extends OrmLiteSqliteOpenHelper {
    private Context context;
    private static final String DATABASE_NAME = "food.db";
    private static final int DATABASE_VERSION = 1;

    public RuntimeExceptionDao simpleDao = null;
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DBHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Product.class);
        } catch (SQLException e) {
            Log.e(DBHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DBHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Product.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DBHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public RuntimeExceptionDao getSimpleDataDao() throws SQLException {
        if (simpleDao == null) {
            simpleDao = getRuntimeExceptionDao(Product.class);
        }
        return simpleDao;
    }

    @Override
    public void close() {
        super.close();
        simpleDao = null;
    }
}