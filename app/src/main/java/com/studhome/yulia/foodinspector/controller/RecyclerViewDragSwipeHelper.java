package com.studhome.yulia.foodinspector.controller;

/**
 * Created by yuliasokolova on 15.02.17.
 */

public interface RecyclerViewDragSwipeHelper {
    //drag item
    boolean onItemMove(int fromPosition, int toPosition);

    //remove item
    void onItemDismiss(int position);
}
