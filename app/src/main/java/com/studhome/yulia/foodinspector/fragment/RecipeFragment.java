package com.studhome.yulia.foodinspector.fragment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.studhome.yulia.foodinspector.adapter.RecipeListAdapter;
import com.studhome.yulia.foodinspector.adapter.RecipeNameListAdapter;
import com.studhome.yulia.foodinspector.activity.CookActivity;
import com.studhome.yulia.foodinspector.activity.MainActivity;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.GetPhotoFirebaseUri;
import com.studhome.yulia.foodinspector.controller.NavigateMenuChangeInterface;
import com.studhome.yulia.foodinspector.controller.OnContextItemClick;
import com.studhome.yulia.foodinspector.controller.OnGetBitmapListener;
import com.studhome.yulia.foodinspector.controller.OnGetUriFileListener;
import com.studhome.yulia.foodinspector.controller.OnReadRecipeList;
import com.studhome.yulia.foodinspector.manager.FbManager;
import com.studhome.yulia.foodinspector.manager.MeManager;
import com.studhome.yulia.foodinspector.model.RecipeModel;
import com.studhome.yulia.foodinspector.view.MenuCircle;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;
import com.vk.sdk.dialogs.VKShareDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Julia on 18.02.2016.
 */
public class RecipeFragment extends Fragment {

    @Bind(R.id.results_recipe_screen)
    LinearLayout yesRecipe;

    @Bind(R.id.empty_recipe)
    LinearLayout noRecipe;

    @Bind(R.id.link_cook)
    TextView link; //// TODO: 03.11.16 do as a clickableSpan

    ListView recipesList, friendsList;

    @Bind(R.id.recipe_content)
    ListView recipeContent;

    @Bind(R.id.tv_recipe_name)
    TextView recipeName;

    @Bind(R.id.tv_weight)
    TextView recipeWeight;

    @Bind(R.id.tv_calorii)
    TextView recipeCalorii;

    @Bind(R.id.tv_belki)
    TextView recipeBelki;

    @Bind(R.id.tv_zuri)
    TextView recipeZuri;

    @Bind(R.id.tv_uglevodi)
    TextView recipeUglevodi;

    @Bind(R.id.btn_share_photo_with_fb)
    ImageView takeAndSharePhotoFb;

    @Bind(R.id.iv_bg_photo)
    ImageView photoReceptIv;

    @Bind(R.id.fb_share_recipe)
    ShareButton shareFbRecipe;

    @Bind(R.id.btn_share_photo_with_vk)
    ImageView shareVkRecipe;

    @Bind(R.id.btn_share_photo_with_tw)
    ImageView shareTwRecipe;

    @Bind(R.id.login_btn_tw)
    TwitterLoginButton loginButtonTwitter;

    @Bind(R.id.top_recipe_menu)
    MenuCircle contextMenu;

    @Bind(R.id.menu_icon)
    LinearLayout menuIcon;

    @Bind(R.id.load_view)
    LinearLayout loadingView;

    private List<RecipeModel> recipes;
    private SlidingMenu slidingMenu;
    private View menuViewRight, menuViewLeft;


    private int currentId = 0;
    private NavigateMenuChangeInterface.OnChangeMenuItem listener;
    private String currentImageUri = "";
    private ShareLinkContent content;
    private CallbackManager callbackManager;
    private RecipeListAdapter recipeListAdapter;
    private View currentView, prevView = null;
    private RecipeModel currentRecipe;
    private final String DEFAULT_IMAGE_URI = "http://www.clker.com/cliparts/a/c/4/9/1325314784688186076coock-hi.png";

    private RecipeNameListAdapter namesAdapter;

    private MainActivity parent;

    private Uri uriLocalImage;

    private boolean editMode = false;
    private boolean isFirstRun = true;


    public static RecipeFragment getInstance(NavigateMenuChangeInterface.OnChangeMenuItem listener) {
        RecipeFragment fragment = new RecipeFragment();
        fragment.listener = listener;
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (listener != null) listener.ChangeMenuItem(R.id.menu_recipe);
        View v = inflater.inflate(R.layout.layout_recipe_fragment, container, false);
        menuViewLeft = inflater.inflate(R.layout.side_bar_recipe_list, null);
        menuViewRight = inflater.inflate(R.layout.side_bar_friend_list, null);
        recipesList = (ListView) menuViewLeft.findViewById(R.id.list_recipes_names);
        friendsList = (ListView) menuViewRight.findViewById(R.id.list_friends);
        ButterKnife.bind(this, v);

        parent = (MainActivity) getActivity();
        showLoadingView();

        initSideMenu();
        initListeners();
        readReceptData();

        return v;
    }


    public void showLoadingView() {
        loadingView.setVisibility(View.VISIBLE);
    }


    public void hideLoadingView() {
        loadingView.setVisibility(View.GONE);
    }

    private void initListeners() {
        parent.showSaveBtn();
        parent.getBtnSaveRecept().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editMode = false;
                parent.startActivity(new Intent(getActivity(), CookActivity.class));
            }
        });

        shareFbRecipe.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(getActivity(), "Successfully posted", Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onCancel() {

            }


            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        yesRecipe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                contextMenu.hideCustomContextMenu();
                return true;
            }
        });

        loadingView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        initLoginTwitterListener();
        initMenuListener();
    }


    private void initMenuListener() {
        contextMenu.setListener(new OnContextItemClick() {
            @Override
            public void onEditClick() {
                editMode = true;
                parent.startActivity(new Intent(getActivity(), CookActivity.class).putExtra(CookActivity.EDIT_PRODUCT, new Gson().toJson(currentRecipe)));
                contextMenu.hideCustomContextMenu();
            }


            @Override
            public void onDelClick() {
                editMode = true;
                new FbManager(getActivity()).deleteRecept(new MeManager(getActivity()).getFbId(), currentRecipe.getKey());
                new FbManager(getActivity()).deletePhoto(currentRecipe.getImageName());
                contextMenu.hideCustomContextMenu();
            }
        });
    }


    private void readReceptData() {
        new FbManager(getActivity()).readReceptListByUserID(new MeManager(getActivity()).getFbId(), new OnReadRecipeList() {
            @Override
            public void OnSuccessRead(ArrayList<RecipeModel> list) {
                recipes = list;
                if (list.size() == 0) {
                    showNoResultsScreen();
                    hideLoadingView();
                } else {
                    showRecipeScreen();
                }
            }
        });
    }


    @OnClick(R.id.btn_share_photo_with_fb)
    void onShareFbClick() {
        shareFbRecipe.performClick();
    }


    @OnClick(R.id.btn_share_photo_with_vk)
    void onShareVk() {
        showLoadingView();
        prepareVkWorking();
        new GetUriOrBitmapFromLink(currentImageUri, null, new OnGetBitmapListener() {
            @Override
            public void onGetBitmap(Bitmap bitmap) {
                if (bitmap == null) {
                    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.kni);
                }
                hideLoadingView();
                callVkShareDialog(bitmap);
            }
        }).execute();
    }


    private void callVkShareDialog(final Bitmap b) {
        final VKShareDialog dialog = new VKShareDialog();
        dialog.setText("Посмотри на мое новое блюдо!\n\n \"" + currentRecipe.getNameRecept() + "\"\n" + getListProductsForSharing())
                .setAttachmentImages(new VKUploadImage[]{
                        new VKUploadImage(b, VKImageParameters.pngImage())
                })
                .setAttachmentLink("FoodInspector", "https://google.com.ua")
                .setShareDialogListener(new VKShareDialog.VKShareDialogListener() {

                    public void onVkShareComplete(int postId) {
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "successfully posted", Toast.LENGTH_SHORT).show();
                        b.recycle();
                    }


                    public void onVkShareCancel() {
                        dialog.dismiss();
                    }


                    @Override
                    public void onVkShareError(VKError error) {
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                    }
                })
                .show(getFragmentManager(), "VK_SHARE_DIALOG");
    }


    @OnClick(R.id.btn_share_photo_with_tw)
    void OnShareTw() {
        showLoadingView();
        final TweetComposer.Builder builder;
        builder = new TweetComposer.Builder(getActivity())
                .text("Look at my new dish!\n \"" + currentRecipe.getNameRecept() + "\"" + " " + "#testTag\n\n" + getListProductsForSharing());

        new GetUriOrBitmapFromLink(currentImageUri, new OnGetUriFileListener() {
            @Override
            public void onGetUri(Uri uri) {
                if (uri != null) {
                    uriLocalImage = uri;
                } else {
                    uriLocalImage = Uri.parse(currentImageUri);
                }
                builder.image(uriLocalImage);
                builder.show();
                hideLoadingView();
            }
        }, null).execute();

    }


    public File getImageLocalFile(String imageUrl) throws IOException {
        String fileName = "local_image.png";
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File localFile = new File(path, "/" + fileName);
        localFile.createNewFile();

        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(localFile, false);

        byte[] b = new byte[32768];
        int length = 0;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
        return localFile;
    }


    private void initLoginTwitterListener() {
        loginButtonTwitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> result) {
                TwitterSession session = result.data;
                Log.d("Twit Token :", " " + result.data.getAuthToken().token);
                Log.d("Twit Secret: ", " " + result.data.getAuthToken().secret);
                Log.d("Twit Name: ", " " + result.data.getUserName());
                // TODO: 21.02.17 save data in  meManager
            }


            @Override
            public void failure(TwitterException exception) {
            }
        });
    }


    private void prepareVkWorking() {
        if (!(VKSdk.wakeUpSession(getActivity()))) {
            String[] scope = new String[]{VKScope.WALL, VKScope.PHOTOS};
            VKSdk.login(getActivity(), scope);
        }

    }


    private void showNoResultsScreen() {
        noRecipe.setVisibility(View.VISIBLE);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.getBtnSaveRecept().performClick();
            }
        });
        yesRecipe.setVisibility(View.GONE);
    }


    private void showRecipeScreen() {
        noRecipe.setVisibility(View.GONE);
        yesRecipe.setVisibility(View.VISIBLE);
        fillRecipesNames();
    }


    @Override
    public void onPause() {
        hideSideMenu();
        makeSideMenuEnable(false);
        super.onPause();

    }


    @Override
    public void onResume() {
        super.onResume();
        makeSideMenuEnable(true);
    }


    private void hideSideMenu() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.toggle();
        }
    }


    public void makeSideMenuEnable(boolean isEnable) {
        if (slidingMenu != null) {
            slidingMenu.setSlidingEnabled(isEnable);
        }
    }


    private void initSideMenu() {
        slidingMenu = new SlidingMenu(getActivity());
        slidingMenu.setMode(SlidingMenu.LEFT_RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.attachToActivity(getActivity(), SlidingMenu.SLIDING_WINDOW | SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setBehindWidthRes(R.dimen.slidingmenu_behind_width);
        slidingMenu.setShadowWidthRes(R.dimen.slidingmenu_shadow_width);
        slidingMenu.setMenu(menuViewRight);
        slidingMenu.setSecondaryMenu(menuViewLeft);
    }


    private void fillRecipesNames() {
        if (isFirstRun) {
            namesAdapter = new RecipeNameListAdapter(getActivity());
            recipesList.setAdapter(namesAdapter);
            recipeListAdapter = new RecipeListAdapter(getActivity());
            recipeContent.setAdapter(recipeListAdapter);
            currentId = 0;
            isFirstRun = false;
        } else {
            if (!editMode) {
                currentId = recipes.size() - 1;
            } else {
                //don't change current position
            }

        }

        namesAdapter.updateList(getRecipeNamesList());


// // TODO: 26.01.17   fix prevView = null
//        if (isFirstRun) {
//            if (!afterNewDish) prevView = namesAdapter.getPrevView();
//            else prevView = namesAdapter.getLastView();
//        }
//        prevView.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));


        currentRecipe = recipes.get(currentId);
        fillRecipeContent(currentRecipe);

//        recipesList.performItemClick(
//                namesAdapter.getView(0, null, null), 0, namesAdapter.getItemId(0));

        recipesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentView = view;
                currentId = position;
                currentView.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));

                if (prevView != null)
                    prevView.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
                prevView = currentView;

                currentRecipe = recipes.get(position);
                fillRecipeContent(currentRecipe);

                hideSideMenu();
            }
        });
        hideLoadingView();
    }


    private void fillRecipeContent(RecipeModel recipeModel) {
        setCurrentImageUri(recipeModel);
        recipeListAdapter.updateRecipeAdapter(recipeModel.getProductList());
        recipeName.setText(recipeModel.getNameRecept());
        recipeWeight.setText(recipeModel.getWeightDish());
        recipeCalorii.setText(recipeModel.getCaloriesDish());
        recipeBelki.setText(recipeModel.getBelkiDish());
        recipeZuri.setText(recipeModel.getZuriDish());
        recipeUglevodi.setText(recipeModel.getUglevodiDish());
    }


    private void setCurrentImageUri(RecipeModel recipeModel) {
        if (recipeModel.getImageName().equals("")) {
            photoReceptIv.setImageResource(R.drawable.kni);
            currentImageUri = DEFAULT_IMAGE_URI;
            initShareFB();
        } else {
            new FbManager(getActivity()).downloadPhotoByPhotoName(recipeModel.getImageName(), photoReceptIv);
            new FbManager(getActivity()).getDownloadUri(recipeModel.getImageName(),
                    new GetPhotoFirebaseUri() {
                        @Override
                        public void onGetImageUri(String uri) {
                            if (uri != null) {
                                currentImageUri = uri;
                                initShareFB();
                            } else {
                                currentImageUri = DEFAULT_IMAGE_URI;
                                photoReceptIv.setImageResource(R.drawable.kni);
                            }
                            hideLoadingView();
                        }
                    });
        }
    }


    private ArrayList<String> getRecipeNamesList() {
        ArrayList<String> names = new ArrayList<>();
        for (RecipeModel model : recipes) {
            names.add(model.getNameRecept());
        }
        return names;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        loginButtonTwitter.onActivityResult(requestCode, resultCode, data);
    }


    private void initShareFB() {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            content = new ShareLinkContent.Builder()
                    .setContentTitle("Look at my new dish!\n \"" + currentRecipe.getNameRecept() + "\"")
                    .setContentUrl(Uri.parse("http://google.com")) // todo put app uri here
                    .setImageUrl(Uri.parse(currentImageUri))
                    .setContentDescription(getListProductsForSharing())
                    .build();
            shareFbRecipe.setShareContent(content);
        }

    }


    private String getListProductsForSharing() {
        StringBuilder stringList = new StringBuilder();
        stringList.append("Состав: ");
        for (int i = 0; i < currentRecipe.getProductList().size(); i++) {
            stringList.append(currentRecipe.getProductList().get(i).getName());
            if (i < currentRecipe.getProductList().size() - 1) stringList.append(", ");
            else stringList.append(".");
        }
        return new String(stringList);
    }


    private class GetUriOrBitmapFromLink extends AsyncTask<Void, Void, File> {
        OnGetUriFileListener uriListener;
        OnGetBitmapListener bitmapListener;
        String imageUrl;


        GetUriOrBitmapFromLink(String imageUrl, OnGetUriFileListener uriListener, OnGetBitmapListener bitmapListener) {
            this.imageUrl = imageUrl;
            this.uriListener = uriListener;
            this.bitmapListener = bitmapListener;
        }


        @Override
        protected File doInBackground(Void... params) {
            File file = null;
            try {
                file = getImageLocalFile(imageUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return file;
        }


        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (uriListener != null) {
                uriListener.onGetUri(Uri.fromFile(file));
            }
            if (bitmapListener != null) {
                bitmapListener.onGetBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));
            }
        }
    }
}
