package com.studhome.yulia.foodinspector.controller;

import com.studhome.yulia.foodinspector.model.Product;

/**
 * Created by yuliasokolova on 03.11.16.
 */
public interface AddNewProduct {
    void onAddNewProduct (Product product);
}
