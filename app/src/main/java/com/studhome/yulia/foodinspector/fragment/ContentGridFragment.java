package com.studhome.yulia.foodinspector.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.studhome.yulia.foodinspector.adapter.CookFragmentGridAdapter;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.GridProductsClickInterface;
import com.studhome.yulia.foodinspector.model.Product;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 12.10.16.
 */
public class ContentGridFragment extends Fragment {
    GridView gridLayout;
    ArrayList<Product> productsList = new ArrayList<>();
    GridProductsClickInterface.OnClickGridItem onClicklistener;

    public static ContentGridFragment getInstance (ArrayList<Product> productsList, GridProductsClickInterface.OnClickGridItem onClicklistener){
        ContentGridFragment fragment = new ContentGridFragment();
        fragment.productsList = productsList;
        fragment.onClicklistener = onClicklistener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_grid_fragment, null);
        gridLayout = (GridView) view.findViewById(R.id.grid_product_list);
        gridLayout.setAdapter(new CookFragmentGridAdapter(getContext(), productsList, onClicklistener));
        return view;
    }

}
