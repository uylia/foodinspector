package com.studhome.yulia.foodinspector.controller;

/**
 * Created by yuliasokolova on 10.03.17.
 */

public interface onUpdateReceptData {
    void getWeight(String value);
    void getCalorii(String value);
    void getBelki(String value);
    void getZuri(String value);
    void getUglevodi(String value);
}
