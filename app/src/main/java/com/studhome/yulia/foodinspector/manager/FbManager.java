package com.studhome.yulia.foodinspector.manager;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.studhome.yulia.foodinspector.controller.GetPhotoFirebaseUri;
import com.studhome.yulia.foodinspector.controller.OnReadProductList;
import com.studhome.yulia.foodinspector.controller.OnReadRecipeList;
import com.studhome.yulia.foodinspector.controller.OnUploadReceptPhoto;
import com.studhome.yulia.foodinspector.model.Product;
import com.studhome.yulia.foodinspector.model.RecipeModel;
import com.studhome.yulia.foodinspector.model.UserModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yuliasokolova on 16.01.17.
 */

public class FbManager {
    private DatabaseReference mDatabase;
    private StorageReference mStorageRef;
    private ArrayList<Product> mProductList;
    private ArrayList<RecipeModel> mRecipeList;
    private ArrayList<UserModel> mUserList;
    private OnReadProductList onReadProductListListener;
    private OnReadRecipeList onReadRecipeListListener;
    private Context context;
    private GetPhotoFirebaseUri onGetPhotoFirebaseUri;
    private OnUploadReceptPhoto uploadPhotoListener;
    private UserManager userManager;


    public FbManager(Context context) {
        this.context = context;
    }


    public void saveRecipe(RecipeModel recipeModel, boolean isEditMode) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        String userId = new MeManager(context).getFbId();
        String key = "";
        if (isEditMode) {
            key = recipeModel.getKey();
        } else {
            key = mDatabase.child(FireBaseConstants.USER_RECEPTS).push().getKey();
        }
        Map<String, Object> postValues = recipeModel.toMap();
        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put(FireBaseConstants.USER_RECEPTS_SLASH + userId + "/" + key, postValues);
        mDatabase.updateChildren(childUpdates);
    }


    public void updateRecipePhoto(String fileName, String key) {
        String userId = new MeManager(context).getFbId();
        FirebaseDatabase.getInstance().getReference().child(FireBaseConstants.USER_RECEPTS_SLASH + userId + "/" + key).child("imageName").setValue(fileName);
    }


    public void readProductList(OnReadProductList onReadProductList) {
        this.onReadProductListListener = onReadProductList;
        mDatabase = FirebaseDatabase.getInstance().getReference(FireBaseConstants.PRODUCT_LIST);
        mProductList = new ArrayList<>();

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Product product = data.getValue(Product.class);
                    mProductList.add(product);
                    Log.d("LOG", "read from firebase product");
                }
                onReadProductListListener.OnSuccessRead(mProductList);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void readReceptListByUserID(String userId, OnReadRecipeList onReadRecipeList) {
        this.onReadRecipeListListener = onReadRecipeList;
        mRecipeList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(FireBaseConstants.USER_RECEPTS_SLASH + userId);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mRecipeList.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    RecipeModel recipe = data.getValue(RecipeModel.class);
                    recipe.setKey(data.getKey());
                    mRecipeList.add(recipe);
                }
                onReadRecipeListListener.OnSuccessRead(mRecipeList);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void deleteRecept(String userId, String receptId){
        mDatabase = FirebaseDatabase.getInstance().getReference(FireBaseConstants.USER_RECEPTS_SLASH + userId).child(receptId);
        mDatabase.removeValue();

    }

    public void deletePhoto(String fileName){
        StorageReference photoRef = FirebaseStorage.getInstance().getReference().child(FireBaseConstants.RECEPT_PHOTOS).child(fileName);
        photoRef.delete();

    }

    public void uploadPhoto(Uri fileUri, String fileName, OnUploadReceptPhoto onUploadPhotoListener) {
        uploadPhotoListener = onUploadPhotoListener;

        mStorageRef = FirebaseStorage.getInstance().getReference();
        final StorageReference photoRef = mStorageRef.child(FireBaseConstants.RECEPT_PHOTOS).child(fileName);
        photoRef.putFile(fileUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if (uploadPhotoListener != null) {
                            uploadPhotoListener.onSuccessUpload();
                        }
                        Log.d("FB STORAGE", "uploadFromUri:onSuccess");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.w("FB STORAGE", "uploadFromUri:onFailure", exception);
                    }
                });

    }


    public void downloadPhotoByPhotoName(String imageName, final ImageView imageView) {
        mStorageRef = FirebaseStorage.getInstance().getReference().child(FireBaseConstants.RECEPT_PHOTOS + imageName);

        mStorageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(context)
                        .using(new FirebaseImageLoader())
                        .load(mStorageRef)
                        .centerCrop()
                        .crossFade()
                        .into(imageView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });
    }

    public void getDownloadUri(String imageName, GetPhotoFirebaseUri getPhotoFirebaseUri){
        mStorageRef = FirebaseStorage.getInstance().getReference().child(FireBaseConstants.RECEPT_PHOTOS + imageName);
        onGetPhotoFirebaseUri = getPhotoFirebaseUri;
        mStorageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                if (onGetPhotoFirebaseUri != null)
                    onGetPhotoFirebaseUri.onGetImageUri(uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                if (onGetPhotoFirebaseUri != null)
                    onGetPhotoFirebaseUri.onGetImageUri(null);
            }
        });
    }


    public void readAllUsers() {
        mUserList = new ArrayList<>();
        userManager = UserManager.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(FireBaseConstants.ALL_USERS);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    UserModel user = data.getValue(UserModel.class);
                    mUserList.add(user);
                    Log.d("LOG", "read from firebase users");
                }
                userManager.setAllUsersList(mUserList);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void addUserToDb(final UserModel user) {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child(FireBaseConstants.ALL_USERS_SLASH + user.getUserFacebookId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    // TODO: handle the case where the data already exists
                } else {
                    // TODO: handle the case where the data does not yet exist
                    String key = user.getUserFacebookId();
                    Map<String, Object> postValues = user.toMap();
                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put(FireBaseConstants.ALL_USERS_SLASH + key, postValues);
                    mDatabase.updateChildren(childUpdates);
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }
}
