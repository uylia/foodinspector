package com.studhome.yulia.foodinspector.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Julia on 29.01.2016.
 */
public class RecipeListAdapter extends BaseAdapter {
    Context context;
    List<Product> product = new ArrayList<>();
    LayoutInflater inflater;

    public RecipeListAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {return product.size();}

    @Override
    public Product getItem(int position) {
        return product.get(position);
    }

    @Override
    public long getItemId(int position) {return position;}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_lv_recipe_list_fragment, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

            Product p = getItem(position);
            holder.name.setText(String.valueOf(p.getName()));
            holder.weight.setText(String.valueOf(p.getWeight()));
            return convertView;
    }

    public void updateRecipeAdapter(ArrayList<Product> products) {
        this.product = products;
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView name, weight;

        public ViewHolder(View v) {
            name = (TextView) v.findViewById(R.id.tv_lv_product_name);
            weight = (TextView) v.findViewById(R.id.tv_lv_product_weight);

        }

    }
}
