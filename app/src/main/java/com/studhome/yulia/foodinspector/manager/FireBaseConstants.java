package com.studhome.yulia.foodinspector.manager;

/**
 * Created by yuliasokolova on 26.01.17.
 */

public interface FireBaseConstants {
    public static final String USER_RECEPTS = "/user-recepts";
    public static final String USER_RECEPTS_SLASH = "/user-recepts/";
    public static final String PRODUCT_LIST = "mass";
    public static final String RECEPT_PHOTOS = "/recipe_photos/";
    public static final String ALL_USERS_SLASH = "/all_users/";
    public static final String ALL_USERS = "/all_users";
}
