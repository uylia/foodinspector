package com.studhome.yulia.foodinspector.activity;


import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.studhome.yulia.foodinspector.adapter.CookFragmentTableAdapter;
import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.AddNewRecept;
import com.studhome.yulia.foodinspector.controller.Dialog;
import com.studhome.yulia.foodinspector.controller.GridProductsClickInterface;
import com.studhome.yulia.foodinspector.controller.OnReadProductList;
import com.studhome.yulia.foodinspector.controller.OnStartDragRecViewListener;
import com.studhome.yulia.foodinspector.controller.OnUploadReceptPhoto;
import com.studhome.yulia.foodinspector.controller.SimpleItemTouchHelperCallback;
import com.studhome.yulia.foodinspector.controller.onUpdateReceptData;
import com.studhome.yulia.foodinspector.fragment.TabProductFragment;
import com.studhome.yulia.foodinspector.manager.FbManager;
import com.studhome.yulia.foodinspector.manager.ProductManager;
import com.studhome.yulia.foodinspector.model.Product;
import com.studhome.yulia.foodinspector.model.RecipeModel;
import com.studhome.yulia.foodinspector.util.GUI;
import com.studhome.yulia.foodinspector.util.GetPhotoUtil;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Julia on 04.02.2016.
 */
public class CookActivity extends AppCompatActivity implements View.OnDragListener, OnStartDragRecViewListener {

    public static final String EDIT_PRODUCT = "edit_product";
    private CookFragmentTableAdapter tableAdapter;
    private Product currentProduct = null;
    private ViewPagerAdapter pagerAdapter = null;
    private ArrayList<Product> mProductFBList;
    private ProductManager productDB;
    private GetPhotoUtil.LoadedPhoto image = null;
    private GetPhotoUtil photoUtil;
    private Dialog addRecipeDialog;
    private AlertDialog dialog;
    private ItemTouchHelper.Callback callback;
    private ItemTouchHelper mItemTouchHelper;
    private RecipeModel editRecipeModel = null;
    private boolean editMode = false;

    @Bind(R.id.lv_for_recipe)
    RecyclerView listProduct;
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;
    @Bind(R.id.pager_main)
    ViewPager pager;
    @Bind(R.id.cook_fragment_content)
    LinearLayout llContent;
    @Bind(R.id.tv_title_toolbar)
    TextView toolbarTitle;
    @Bind(R.id.iv_toolbar_save)
    ImageView toolbarSaveBtn;
    @Bind(R.id.iv_back_toolbar)
    ImageView toolbarBackBtn;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.load_view)
    LinearLayout loadingView;
    @Bind(R.id.sum_weight)
    TextView tvWeight;
    @Bind(R.id.sum_calories)
    TextView tvCalories;
    @Bind(R.id.sum_zuri)
    TextView tvZuri;
    @Bind(R.id.sum_belki)
    TextView tvBelki;
    @Bind(R.id.sum_uglevodi)
    TextView tvUglevodi;
    @Bind(R.id.iv_bottom_sheet_icon)
    ImageView ivBottomIcon;

    /* bottom sheet components */
    @Bind(R.id.bottom_sheet)
    View bottomSheet;

    @Bind(R.id.hide_bottom_sheet_layout)
    CoordinatorLayout viewForHideBottomSheet;

    private BottomSheetBehavior bottomSheetBehavior;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cook_activity);
        ButterKnife.bind(this);

        initListeners();
        initExtras();
        new FbManager(this).readProductList(new OnReadProductList() {
            @Override
            public void OnSuccessRead(ArrayList<Product> list) {
                mProductFBList = list;
                productDB = new ProductManager(CookActivity.this);//read my own added products
                mProductFBList.addAll(productDB.getProductList());
                initKeyBoardHiding();
                initTabsWithPager();
                initLoadingView();
                initReceptTable();
                initSaveBtnListener();
                initCallBackRecView();
                initEditReceptData();
                initToolbar();
            }
        });
        setupBottomSheet();
    }


    private void initCallBackRecView() {
        callback = new SimpleItemTouchHelperCallback(tableAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(listProduct);
    }


    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        if (editRecipeModel == null) {
            toolbarTitle.setText(R.string.cook_title);
        } else {
            toolbarTitle.setText(R.string.edit_title);
        }

    }


    private void initListeners() {
        photoUtil = new GetPhotoUtil(this, new GetPhotoUtil.PhotoUtilListener() {
            @Override
            public void onImageDecoding() {
            }


            @Override
            public void onPicObtained(GetPhotoUtil.LoadedPhoto loadedPhoto) {
                if (loadedPhoto != null) {
                    image = loadedPhoto;
                    if (addRecipeDialog != null)
                        addRecipeDialog.setImage(image);
                }
            }
        });
    }


    private void setupBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setHideable(false);
        bottomSheetBehavior.setPeekHeight(150);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        viewForHideBottomSheet.setVisibility(View.GONE);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    default:
                        viewForHideBottomSheet.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        viewForHideBottomSheet.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                return false;
            }
        });

        ivBottomIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });
    }


    private void initExtras() {
        if (getIntent().getExtras() != null) {
            String json = getIntent().getExtras().getString(EDIT_PRODUCT, "");
            editRecipeModel = new Gson().fromJson(json, RecipeModel.class);
            editMode = true;
        } else {
            editMode = false;
        }
    }


    private void initKeyBoardHiding() {
        llContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (GUI.isKeyboardVisible(v, CookActivity.this)) {
                    GUI.hideKeyboard(CookActivity.this);
                    return true;
                }
                return false;
            }
        });
    }


    private void initEditReceptData() {
        if (!editMode) return;

        tableAdapter.addListItem(editRecipeModel.getProductList());
        fillDetails();
    }


    private void fillDetails() {
        tvWeight.setText(editRecipeModel.getWeightDish());
        tvCalories.setText(editRecipeModel.getCaloriesDish());
        tvBelki.setText(editRecipeModel.getBelkiDish());
        tvZuri.setText(editRecipeModel.getZuriDish());
        tvUglevodi.setText(editRecipeModel.getUglevodiDish());
    }


    private void initLoadingView() {
        loadingView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }


    @OnClick(R.id.iv_back_toolbar)
    void onBackClick() {
        onBackPressed();
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    private void initReceptTable() {
        listProduct.setOnDragListener(this);
        tableAdapter = new CookFragmentTableAdapter(CookActivity.this);
        tableAdapter.setUiListener(new onUpdateReceptData() {
            @Override
            public void getWeight(String value) {
                tvWeight.setText(value);
            }


            @Override
            public void getCalorii(String value) {
                tvCalories.setText(value);
            }


            @Override
            public void getBelki(String value) {
                tvBelki.setText(value);
            }


            @Override
            public void getZuri(String value) {
                tvZuri.setText(value);
            }


            @Override
            public void getUglevodi(String value) {
                tvUglevodi.setText(value);
            }
        });

        listProduct.setLayoutManager(new LinearLayoutManager(CookActivity.this));
        listProduct.setAdapter(tableAdapter);
    }


    private void initSaveBtnListener() {
        toolbarSaveBtn.setVisibility(View.VISIBLE);
        toolbarSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tableAdapter.getItemCount() == 0) {
                    Toast.makeText(CookActivity.this, "Add some products first", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    addRecipeDialog = new Dialog(CookActivity.this);
                    addRecipeDialog.getAddReceptDialog(prepareRecipeModel(),
                            new AddNewRecept() {
                                @Override
                                public void onAddNewRecept(RecipeModel recipeModel) {
                                    loadingView.setVisibility(View.VISIBLE);
                                    uploadPhotoToFb(recipeModel);
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog = new Dialog(CookActivity.this).getPickPhotoDialog(
                                            new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    photoUtil.takeFromCamera();
                                                    dialog.dismiss();
                                                }
                                            },
                                            new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    photoUtil.takeFromGallery();
                                                    dialog.dismiss();
                                                }
                                            });
                                }
                            });
                }
            }
        });
    }


    private void uploadPhotoToFb(final RecipeModel recipeModel) {
        if (image != null) {
            if (!recipeModel.getImageName().equals("")) {
                //remove old image in storege
                new FbManager(CookActivity.this).deletePhoto(recipeModel.getImageName());
                //update image name in database
                new FbManager(CookActivity.this).updateRecipePhoto(image.file.getName(), recipeModel.getKey());
            }
            //upload to storage
            new FbManager(CookActivity.this).uploadPhoto(image.uri, image.file.getName(), new OnUploadReceptPhoto() {
                @Override
                public void onSuccessUpload() {
                    recipeModel.setImageName(image.file.getName());
                    addNewRecipeToFb(recipeModel);
                    loadingView.setVisibility(View.GONE);
                    finish();
                }
            });

        } else {
            addNewRecipeToFb(recipeModel);
            loadingView.setVisibility(View.GONE);
            finish();
        }
    }


    private void addNewRecipeToFb(RecipeModel recipeModel) {
        new FbManager(CookActivity.this).saveRecipe(recipeModel, editMode);
    }


    private RecipeModel prepareRecipeModel() {
        RecipeModel recipeModel = new RecipeModel();
        recipeModel.setCaloriesDish(tableAdapter.getDishCalories());
        recipeModel.setWeightDish(tableAdapter.getDishWeight());
        recipeModel.setBelkiDish(tableAdapter.getDishBelki());
        recipeModel.setZuriDish(tableAdapter.getDishZuri());
        recipeModel.setUglevodiDish(tableAdapter.getDishUglevodi());
        recipeModel.setProductList(tableAdapter.getMyList());
        if (editRecipeModel != null) {
            recipeModel.setImageName(editRecipeModel.getImageName());
            recipeModel.setNameRecept(editRecipeModel.getNameRecept());
            recipeModel.setKey(editRecipeModel.getKey());
        }
        return recipeModel;
    }


    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (currentProduct == null) return false;

        v = (View) event.getLocalState();
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                v.setAlpha(0.3f);
                v.setEnabled(false);
                // currentBg.setImageResource(R.drawable.white_bg);
                tableAdapter.addNewItem(currentProduct);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                break;
            default:
                break;
        }
        return true;
    }


    private void setTabs() {
        int[] icons = {R.drawable.tab_fruits,
                R.drawable.tab_fruits,
                R.drawable.tab_milk,
                R.drawable.tab_groats,
                R.drawable.tab_beef};
        String[] names = getResources().getStringArray(R.array.categories);
        for (int i = 0; i < 5; i++) {
            tabLayout.addTab(tabLayout.newTab().setTag(i).setText(names[i]).setIcon(icons[i]));
        }
    }


    public void initTabsWithPager() {
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        setTabs();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tag = (int) tab.getTag();
                pager.setCurrentItem(tag, true);
            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }


            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }


            @Override
            public void onPageSelected(int position) {
                if (!tabLayout.getTabAt(position).isSelected())
                    tabLayout.getTabAt(position).select();
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setCurrentItem(0);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GetPhotoUtil.REQ_GALLERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    photoUtil.sendGallery();
                } else {
                    Log.e("Exeption", "Permission Denied");
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        photoUtil.resolveResult(requestCode, resultCode, data);
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter implements GridProductsClickInterface.OnClickGridItem {
        private final int PAGER_SIZE = 5;


        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return TabProductFragment.getInstance(position, this, mProductFBList);
        }


        @Override
        public int getCount() {
            return PAGER_SIZE;
        }


        @Override
        public void onClick(Product product, CircleImageView bg, View view) {
            currentProduct = product;
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
        }

    }
}
