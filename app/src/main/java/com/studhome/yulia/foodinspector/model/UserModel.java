package com.studhome.yulia.foodinspector.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yuliasokolova on 09.02.17.
 */

public class UserModel {
    private String userFacebookId;
    private String userName;
    private String userImageUrl;
    private String userEmail;
    //private boolean isMyFriend;


    public UserModel(){}

    public String getUserFacebookId() {
        return userFacebookId;
    }


    public void setUserFacebookId(String userFacebookId) {
        this.userFacebookId = userFacebookId;
    }


    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getUserEmail() {
        return userEmail;
    }


    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }


    public String getUserImageUrl() {
        return userImageUrl;
    }


    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

//
//    public boolean isMyFriend() {
//        return isMyFriend;
//    }
//
//
//    public void setMyFriend(boolean myFriend) {
//        isMyFriend = myFriend;
//    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("token", userFacebookId);
        result.put("name", userName);
        result.put("imageUrl", userImageUrl);
        result.put("email", userEmail);
    //    result.put("isMyFriend", isMyFriend);
        return result;
    }
}
