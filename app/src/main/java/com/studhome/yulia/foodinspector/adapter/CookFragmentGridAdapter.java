package com.studhome.yulia.foodinspector.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.studhome.yulia.foodinspector.R;
import com.studhome.yulia.foodinspector.controller.GridProductsClickInterface;
import com.studhome.yulia.foodinspector.model.Product;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by uylia on 12.10.2016.
 */
//for grid view
public class CookFragmentGridAdapter extends BaseAdapter {
    ArrayList<Product> products;
    LayoutInflater inflater;
    Context context;
    GridProductsClickInterface.OnClickGridItem onClicklistener;


    public CookFragmentGridAdapter(Context context, ArrayList<Product> products, GridProductsClickInterface.OnClickGridItem onClicklistener) {
        this.context = context;
        this.products = new ArrayList();
        this.products.addAll(products);
        this.onClicklistener = onClicklistener;
        inflater = LayoutInflater.from(this.context);
    }


    @Override
    public int getCount() {
        return products.size();
    }


    @Override
    public Product getItem(int position) {
        return products.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.tab_product_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (MyViewHolder) convertView.getTag();


        final Product item = getItem(position);

        mViewHolder.name.setText(item.getName());
//        String imageUrl = item.getImage_url();
//        String source = "com.studhome.yulia.foodinspector:drawable/" + imageUrl.substring(11, imageUrl.length());
//        int imageId = context.getResources()
//                .getIdentifier(source, null, null);
        int imageId = R.drawable.tab_fruits;
        if (item.getCategor().equals("3")) imageId = R.drawable.tab_milk;
        if (item.getCategor().equals("4")) imageId = R.drawable.tab_groats;
        if (item.getCategor().equals("5")) imageId = R.drawable.tab_beef;

        mViewHolder.iv_product.setImageResource(imageId);

//        convertView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    if (onClicklistener != null) {
//                        onClicklistener.onClick(item, mViewHolder.bg_product, view);
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (onClicklistener != null) {
                     onClicklistener.onClick(item, mViewHolder.bg_product, view);
                     return true;
                    }

                return false;
            }
        });
        return convertView;
    }


    private class MyViewHolder {
        TextView name;
        CircleImageView bg_product;
        ImageView iv_product;


        public MyViewHolder(View item) {
            name = (TextView) item.findViewById(R.id.tv_product_name);
            bg_product = (CircleImageView) item.findViewById(R.id.civ_bg_product);
            iv_product = (ImageView) item.findViewById(R.id.iv_product);

        }

    }
}
