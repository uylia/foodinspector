package com.studhome.yulia.foodinspector.controller;

import android.support.v7.widget.RecyclerView;

/**
 * Created by yuliasokolova on 15.02.17.
 */

public interface OnStartDragRecViewListener {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
