// Generated code from Butter Knife. Do not modify!
package com.studhome.yulia.foodinspector.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductListFragment$$ViewBinder<T extends com.studhome.yulia.foodinspector.fragment.ProductListFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689682, "field 'listView'");
    target.listView = finder.castView(view, 2131689682, "field 'listView'");
    view = finder.findRequiredView(source, 2131689683, "field 'fab' and method 'onFABClick'");
    target.fab = finder.castView(view, 2131689683, "field 'fab'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onFABClick();
        }
      });
  }

  @Override public void unbind(T target) {
    target.listView = null;
    target.fab = null;
  }
}
