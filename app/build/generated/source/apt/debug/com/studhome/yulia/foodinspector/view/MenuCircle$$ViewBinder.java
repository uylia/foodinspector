// Generated code from Butter Knife. Do not modify!
package com.studhome.yulia.foodinspector.view;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MenuCircle$$ViewBinder<T extends com.studhome.yulia.foodinspector.view.MenuCircle> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689722, "field 'menuIcon' and method 'onContextIconClick'");
    target.menuIcon = finder.castView(view, 2131689722, "field 'menuIcon'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onContextIconClick();
        }
      });
    view = finder.findRequiredView(source, 2131689723, "field 'menuLayout'");
    target.menuLayout = finder.castView(view, 2131689723, "field 'menuLayout'");
    view = finder.findRequiredView(source, 2131689724, "field 'tvMenuEdit' and method 'onEditClick'");
    target.tvMenuEdit = finder.castView(view, 2131689724, "field 'tvMenuEdit'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onEditClick();
        }
      });
    view = finder.findRequiredView(source, 2131689725, "field 'getTvMenuDelete' and method 'onDeleteClick'");
    target.getTvMenuDelete = finder.castView(view, 2131689725, "field 'getTvMenuDelete'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onDeleteClick();
        }
      });
  }

  @Override public void unbind(T target) {
    target.menuIcon = null;
    target.menuLayout = null;
    target.tvMenuEdit = null;
    target.getTvMenuDelete = null;
  }
}
