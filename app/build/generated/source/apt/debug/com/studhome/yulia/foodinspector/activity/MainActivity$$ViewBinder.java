// Generated code from Butter Knife. Do not modify!
package com.studhome.yulia.foodinspector.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivity$$ViewBinder<T extends com.studhome.yulia.foodinspector.activity.MainActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689610, "field 'shareButton'");
    target.shareButton = finder.castView(view, 2131689610, "field 'shareButton'");
    view = finder.findRequiredView(source, 2131689716, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131689716, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131689717, "field 'title'");
    target.title = finder.castView(view, 2131689717, "field 'title'");
    view = finder.findRequiredView(source, 2131689608, "field 'drawerLayout'");
    target.drawerLayout = finder.castView(view, 2131689608, "field 'drawerLayout'");
    view = finder.findRequiredView(source, 2131689609, "field 'navigationView'");
    target.navigationView = finder.castView(view, 2131689609, "field 'navigationView'");
    view = finder.findRequiredView(source, 2131689718, "field 'ivToolbarSave'");
    target.ivToolbarSave = finder.castView(view, 2131689718, "field 'ivToolbarSave'");
    view = finder.findRequiredView(source, 2131689625, "field 'loadingView'");
    target.loadingView = finder.castView(view, 2131689625, "field 'loadingView'");
  }

  @Override public void unbind(T target) {
    target.shareButton = null;
    target.toolbar = null;
    target.title = null;
    target.drawerLayout = null;
    target.navigationView = null;
    target.ivToolbarSave = null;
    target.loadingView = null;
  }
}
