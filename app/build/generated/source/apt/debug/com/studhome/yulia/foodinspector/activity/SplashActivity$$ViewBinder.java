// Generated code from Butter Knife. Do not modify!
package com.studhome.yulia.foodinspector.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SplashActivity$$ViewBinder<T extends com.studhome.yulia.foodinspector.activity.SplashActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689750, "field 'loginButton'");
    target.loginButton = finder.castView(view, 2131689750, "field 'loginButton'");
    view = finder.findRequiredView(source, 2131689749, "field 'loadingView'");
    target.loadingView = finder.castView(view, 2131689749, "field 'loadingView'");
  }

  @Override public void unbind(T target) {
    target.loginButton = null;
    target.loadingView = null;
  }
}
