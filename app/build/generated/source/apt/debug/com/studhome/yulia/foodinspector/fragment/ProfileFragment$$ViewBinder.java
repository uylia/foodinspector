// Generated code from Butter Knife. Do not modify!
package com.studhome.yulia.foodinspector.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProfileFragment$$ViewBinder<T extends com.studhome.yulia.foodinspector.fragment.ProfileFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689685, "field 'ivProfileImage'");
    target.ivProfileImage = finder.castView(view, 2131689685, "field 'ivProfileImage'");
    view = finder.findRequiredView(source, 2131689686, "field 'etName'");
    target.etName = finder.castView(view, 2131689686, "field 'etName'");
    view = finder.findRequiredView(source, 2131689687, "field 'etAge'");
    target.etAge = finder.castView(view, 2131689687, "field 'etAge'");
    view = finder.findRequiredView(source, 2131689688, "field 'etHeight'");
    target.etHeight = finder.castView(view, 2131689688, "field 'etHeight'");
    view = finder.findRequiredView(source, 2131689689, "field 'etWeight'");
    target.etWeight = finder.castView(view, 2131689689, "field 'etWeight'");
    view = finder.findRequiredView(source, 2131689684, "field 'cardEditText'");
    target.cardEditText = finder.castView(view, 2131689684, "field 'cardEditText'");
    view = finder.findRequiredView(source, 2131689690, "field 'cardRadioBtn'");
    target.cardRadioBtn = finder.castView(view, 2131689690, "field 'cardRadioBtn'");
    view = finder.findRequiredView(source, 2131689698, "field 'btnCalculate' and method 'onCalculateClick'");
    target.btnCalculate = finder.castView(view, 2131689698, "field 'btnCalculate'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onCalculateClick();
        }
      });
    view = finder.findRequiredView(source, 2131689691, "field 'rbFemale'");
    target.rbFemale = finder.castView(view, 2131689691, "field 'rbFemale'");
    view = finder.findRequiredView(source, 2131689692, "field 'rbMale'");
    target.rbMale = finder.castView(view, 2131689692, "field 'rbMale'");
    view = finder.findRequiredView(source, 2131689699, "field 'tvDailyCaloriesNorm'");
    target.tvDailyCaloriesNorm = finder.castView(view, 2131689699, "field 'tvDailyCaloriesNorm'");
    view = finder.findRequiredView(source, 2131689700, "field 'tvDailyCaloriesLose'");
    target.tvDailyCaloriesLose = finder.castView(view, 2131689700, "field 'tvDailyCaloriesLose'");
  }

  @Override public void unbind(T target) {
    target.ivProfileImage = null;
    target.etName = null;
    target.etAge = null;
    target.etHeight = null;
    target.etWeight = null;
    target.cardEditText = null;
    target.cardRadioBtn = null;
    target.btnCalculate = null;
    target.rbFemale = null;
    target.rbMale = null;
    target.tvDailyCaloriesNorm = null;
    target.tvDailyCaloriesLose = null;
  }
}
