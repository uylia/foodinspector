// Generated code from Butter Knife. Do not modify!
package com.studhome.yulia.foodinspector.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RecipeFragment$$ViewBinder<T extends com.studhome.yulia.foodinspector.fragment.RecipeFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689702, "field 'yesRecipe'");
    target.yesRecipe = finder.castView(view, 2131689702, "field 'yesRecipe'");
    view = finder.findRequiredView(source, 2131689701, "field 'noRecipe'");
    target.noRecipe = finder.castView(view, 2131689701, "field 'noRecipe'");
    view = finder.findRequiredView(source, 2131689729, "field 'link'");
    target.link = finder.castView(view, 2131689729, "field 'link'");
    view = finder.findRequiredView(source, 2131689674, "field 'recipeContent'");
    target.recipeContent = finder.castView(view, 2131689674, "field 'recipeContent'");
    view = finder.findRequiredView(source, 2131689703, "field 'recipeName'");
    target.recipeName = finder.castView(view, 2131689703, "field 'recipeName'");
    view = finder.findRequiredView(source, 2131689677, "field 'recipeWeight'");
    target.recipeWeight = finder.castView(view, 2131689677, "field 'recipeWeight'");
    view = finder.findRequiredView(source, 2131689678, "field 'recipeCalorii'");
    target.recipeCalorii = finder.castView(view, 2131689678, "field 'recipeCalorii'");
    view = finder.findRequiredView(source, 2131689706, "field 'recipeBelki'");
    target.recipeBelki = finder.castView(view, 2131689706, "field 'recipeBelki'");
    view = finder.findRequiredView(source, 2131689707, "field 'recipeZuri'");
    target.recipeZuri = finder.castView(view, 2131689707, "field 'recipeZuri'");
    view = finder.findRequiredView(source, 2131689708, "field 'recipeUglevodi'");
    target.recipeUglevodi = finder.castView(view, 2131689708, "field 'recipeUglevodi'");
    view = finder.findRequiredView(source, 2131689710, "field 'takeAndSharePhotoFb' and method 'onShareFbClick'");
    target.takeAndSharePhotoFb = finder.castView(view, 2131689710, "field 'takeAndSharePhotoFb'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onShareFbClick();
        }
      });
    view = finder.findRequiredView(source, 2131689704, "field 'photoReceptIv'");
    target.photoReceptIv = finder.castView(view, 2131689704, "field 'photoReceptIv'");
    view = finder.findRequiredView(source, 2131689709, "field 'shareFbRecipe'");
    target.shareFbRecipe = finder.castView(view, 2131689709, "field 'shareFbRecipe'");
    view = finder.findRequiredView(source, 2131689711, "field 'shareVkRecipe' and method 'onShareVk'");
    target.shareVkRecipe = finder.castView(view, 2131689711, "field 'shareVkRecipe'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onShareVk();
        }
      });
    view = finder.findRequiredView(source, 2131689713, "field 'shareTwRecipe' and method 'OnShareTw'");
    target.shareTwRecipe = finder.castView(view, 2131689713, "field 'shareTwRecipe'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.OnShareTw();
        }
      });
    view = finder.findRequiredView(source, 2131689712, "field 'loginButtonTwitter'");
    target.loginButtonTwitter = finder.castView(view, 2131689712, "field 'loginButtonTwitter'");
    view = finder.findRequiredView(source, 2131689705, "field 'contextMenu'");
    target.contextMenu = finder.castView(view, 2131689705, "field 'contextMenu'");
    view = finder.findRequiredView(source, 2131689722, "field 'menuIcon'");
    target.menuIcon = finder.castView(view, 2131689722, "field 'menuIcon'");
    view = finder.findRequiredView(source, 2131689625, "field 'loadingView'");
    target.loadingView = finder.castView(view, 2131689625, "field 'loadingView'");
  }

  @Override public void unbind(T target) {
    target.yesRecipe = null;
    target.noRecipe = null;
    target.link = null;
    target.recipeContent = null;
    target.recipeName = null;
    target.recipeWeight = null;
    target.recipeCalorii = null;
    target.recipeBelki = null;
    target.recipeZuri = null;
    target.recipeUglevodi = null;
    target.takeAndSharePhotoFb = null;
    target.photoReceptIv = null;
    target.shareFbRecipe = null;
    target.shareVkRecipe = null;
    target.shareTwRecipe = null;
    target.loginButtonTwitter = null;
    target.contextMenu = null;
    target.menuIcon = null;
    target.loadingView = null;
  }
}
